# uninstall_list
 
function uninstall() {
	echo "pm uninstall " $1
	pm uninstall $1
	echo "pm uninstall --user 0 " $1
	pm uninstall --user 0 $1
	echo "************************************************************************"
}

function disable() {
	echo "pm disable-user --user 0" $1
	pm disable-user --user 0 $1
	echo "************************************************************************"
}

echo "Analytics"
uninstall com.miui.analytics

echo "Android Auto"
uninstall com.google.android.projection.gearhead

echo "App Vault"
uninstall com.mi.globalminusscreen

echo "Assistant"
uninstall com.google.android.apps.googleassistant

echo "Calculator"
uninstall com.miui.calculator

echo "Calendar"
uninstall com.xiaomi.calendar

echo "Calendar Services"
uninstall com.android.calendar

# echo "Carrier Services"
# uninstall com.google.android.ims

echo "Cast"
uninstall com.milink.service

echo "Casting"
uninstall com.xiaomi.miplay_client

echo "Cell Broadcast Service"
uninstall com.google.android.cellbroadcastservice

echo "Cloud Backup"
uninstall com.miui.cloudbackup

echo "CloudServiceSysbase"
uninstall com.miui.cloudservice.sysbase

echo "Compass"
uninstall com.miui.compass

echo "Data restore tool"
uninstall com.google.android.apps.restore

echo "Device Health Services"
uninstall com.google.android.apps.turbo

echo "Digital Wellbeing"
uninstall com.google.android.apps.wellbeing

echo "Downloads UI"
uninstall com.android.providers.downloads.ui

echo "Emoji Keyboard"
disable com.facemoji.lite.xiaomi

echo "Facebook App Installer"
uninstall com.facebook.system

echo "Facebook App Manager"
uninstall com.facebook.appmanager

echo "Facebook Services"
uninstall com.facebook.services

echo "Feedback"
uninstall com.miui.bugreport

echo "Frequent phrases"
uninstall com.miui.phrase

echo "Google"
uninstall com.google.android.googlequicksearchbox

echo "Google Assistant"
uninstall com.android.hotwordenrollment.xgoogle

echo "Google Assistant"
uninstall com.android.hotwordenrollment.okgoogle

echo "Google One"
uninstall com.google.android.apps.subscriptions.red

echo "Heart Rate"
uninstall com.mi.healthglobal

echo "Joyose"
uninstall com.xiaomi.joyose

echo "Log generator"
uninstall com.bsp.catchlog

echo "Market Feedback Agent"
uninstall com.google.android.feedback

echo "Mi Coin"
uninstall com.xiaomi.payment

echo "Mi Connect"
uninstall com.xiaomi.mi_connect_service

echo "Mi Pay"
uninstall com.mipay.wallet.in

echo "Mi Share"
uninstall com.miui.mishare.connectivity

echo "MiCloudSync"
uninstall com.miui.micloudsync

echo "MipayService"
uninstall org.mipay.android.manager

echo "MIUI Daemon"
uninstall com.miui.daemon

echo "msa"
uninstall com.miui.msa.global

echo "Music"
uninstall com.miui.player

echo "Notes"
uninstall com.miui.notes

echo "PlayAutoInstalls"
uninstall android.autoinstalls.config.Xiaomi.qssi

echo "Quick apps Service Framework"
uninstall com.miui.hybrid

echo "Scanner"
uninstall com.xiaomi.scanner

echo "Services & feedback"
uninstall com.miui.miservice

echo "ShareMe"
uninstall com.xiaomi.midrop

echo "SIM toolkit"
uninstall com.android.stk

echo "Smart Scenes"
uninstall com.miui.hybrid.accessory

echo "SoterService"
uninstall com.tencent.soter.soterserver

echo "System Tracing"
uninstall com.android.traceur

echo "Weather"
uninstall com.miui.weather2

echo "WMService"
uninstall com.miui.wmsvc

echo "Xiaomi Cloud"
uninstall com.miui.cloudservice

echo "Xiaomi service framework"
uninstall com.xiaomi.xmsf

echo "Xiaomi Service Framework Keeper"
uninstall com.xiaomi.xmsfkeeper

echo "Xiaomi SIM Activation Service"
uninstall com.xiaomi.simactivate.service

echo "Yellow pages"
uninstall com.miui.yellowpage

echo "com.xiaomi.micloudsdk.SdkApplication"
uninstall com.xiaomi.micloud.sdk

# echo "Find device"
# uninstall com.xiaomi.finddevice

# echo "Xiaomi Account"
# uninstall com.xiaomi.account

echo "File Manager"
disable com.mi.android.globalFileexplorer

echo "Bookmark Provider"
uninstall com.android.bookmarkprovider

echo "Basic Daydreams"
uninstall com.android.dreams.basic

echo "Support components"
uninstall com.google.mainline.telemetry

echo "Live Wallpaper Picker"
uninstall com.android.wallpaper.livepicker

echo "Get Apps"
uninstall com.xiaomi.mipicks

echo "Android Accessibility Suite"
uninstall com.google.android.marvin.talkback

echo "Cleaner"
uninstall com.miui.cleaner

echo "Drive"
uninstall com.google.android.apps.docs

echo "Facebook"
uninstall com.facebook.katana

echo "Game Center"
uninstall com.xiaomi.glgm

echo "Gmail"
uninstall com.google.android.gm

echo "Google News"
uninstall com.google.android.apps.magazines

echo "Google PLay Services for AR"
uninstall com.google.ar.core

echo "Google Podcasts"
uninstall com.google.android.apps.podcasts

echo "Google TV"
uninstall com.google.android.videos

echo "Gpay"
uninstall com.google.android.apps.nbu.paisa.user

echo "Home"
uninstall com.google.android.apps.chromecast.app

echo "Linkedin"
uninstall com.linkedin.android

echo "Maps"
uninstall com.google.android.apps.maps

echo "Meet"
uninstall com.google.android.apps.tachyon

echo "Mi Doc Viewer"
uninstall cn.wps.xiaomi.abroad.lite

echo "Mi Shop"
uninstall com.mi.global.shop

echo "Opera Browser"
uninstall com.opera.browser.afin

echo "Photos"
uninstall com.google.android.apps.photos

echo "Wallpaper Carousel"
uninstall com.miui.android.fashiongallery

